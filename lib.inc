section .text
 
 
; Accepts the return code and completes the current process
exit: 
	mov rax, 60
	syscall
	
 

; Takes a pointer to a null-terminated string, returns its length
string_length:
mov rax, 0	; clearing rax so that it has zero
	.loop:
		cmp byte[rdi+rax], 0	;comparing the symbol with the null terminator
		je .end			;if zero is the terminator, exit
		inc rax			;increment rax to check the next simbol
		jmp .loop
	.end:
		ret	


; Takes a pointer to a null-terminated string, outputs it to stdout
print_string:
	call string_length	; in rax - length of the string	
	mov rdx, rax		; in rdx put the length of the string
	mov rsi, rdi		; in the rsi put the address of the beginning of the line
	mov rdi, 1		; in rdi put the stream descriptor of the output stream file		
	mov rax, 1		; in rax put the number of the write siscoll
	syscall
	xor rax, rax
	ret

; Changes the string (outputs the simbol with the code 0xA)
print_newline:
	mov rdi , 10
	

; Accepts the simbol code and outputs it to stdout
print_char:
    push rdi
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret



; Outputs an unsigned 8-byte number in decimal format
print_uint:
	mov r9, rsp ;put the address of the top of the stack in r9
	mov rax, rdi ;put the number to be translated into rax
	mov rcx, 10  ;use rcx as a divisor
	dec rsp;
	mov byte[rsp], 0
.loop:
	xor rdx, rdx
	div rcx ; divide rax by 10, the remainder of the division is written to rdx
	or rdx, 0x30
	dec rsp
	mov byte[rsp], dl ; saving the digit to the stack
	test rax, rax ; measure the number to see if it is over
	jnz .loop

	mov rdi, rsp
	push r9
	call print_string
	pop r9
	mov rsp, r9
	ret



; Outputs a signed 8-byte number in decimal format
print_int:
	test rdi, rdi
	jns .uint
	push rdi
	mov rdi, "-"
	call print_char
	pop rdi
	neg rdi
.uint:
	call print_uint
	ret


; Accepts two pointers to null-terminated strings, returns 1 if they are equal, 0 otherwise
string_equals:

; in rdi - a pointer to the first line, in rsi - to the second

	call string_length ; counting the length of the first line
	mov r9 , rax	  ; put its length in r9
	xchg rdi, rsi	  ; put a pointer to the second string in rdi in order to calculate its length in string_length
	call string_length ; counting the length of the second line
	cmp r9, rax	  ; compare the lengths of two strings , if they are not equal - to the output, if they are equal - we begin to compare the strings character by character
	jne .notequals
	xor rax, rax
.loop:
	mov al, byte[rdi] ; put the rax symbol in the lowest byte
	cmp al, byte[rsi] ; compare the symbol of the first line with the symbol of the second line
	jne .notequals    ; if they are not equal, then we exit
	cmp al, 0	 ; since the symbols are equal, it is enough to check only one of them for a null terminator
	je .end	         ; if zero is a terminator, then we have checked both lines and they are equal - exit and return 1
	inc rdi           ; increment the rdi to read the next character
	inc rsi		 ; increment the rsi to read the next character
	jmp .loop

.notequals:
	xor rax, rax
	ret	
.end:
	mov rax, 1
	ret
	
    

; Reads one character from stdin and returns it. Returns 0 if the end of the stream is reached
read_char:
	xor rax, rax ;syscall read number
	xor rdi, rdi ;stdin file descriptor
	mov rdx, 1 ;length
	push 0	  ; reserve a place for reading
	mov rsi, rsp ; the address where the symbol will be read
	syscall
	pop rax	     ; read the entered character into rax
	ret	
   
   
; the function must add a null terminator to the word
read_word:
	push r13 ;push the calle-saved registers to restore them later
	push r14 
	push r15
	mov r13, rdi ; put the address of the beginning of the buffer in r13
	mov r14, rsi ; put the buffer size in r14
	xor r15, r15 ;counter of read characters

.first_read:
	call read_char
	cmp rax, 0x20 ;check for a space character
	je .first_read
	cmp rax, 0x9  ;check for a space character
	je .first_read
	cmp rax, 0xA  ;check for a space character
	je .first_read
	cmp rax, 0x0  ;if the first character is a zero terminator
	je .end
	
.continue_read:
	cmp r14, r15 ; check whether we have exceeded the buffer size
	jbe .overflow
	mov byte[r13+r15], al ; put the first character in the buffer
	cmp al, 0x20
	je .end
	cmp al, 0x9
	je .end
	cmp al, 0xA
	je .end
	cmp al, 0x0
	je .end
	inc r15
	cmp r15, r14
	je .overflow
	call read_char 
	jmp .continue_read

.overflow:
	xor rax, rax
	pop r15
	pop r14
	pop r13
	ret
.end:
	mov byte[r13+r15] , 0
	mov rax, r13
	mov rdx, r15
	pop r15
	pop r14
	pop r13
	ret	
		 

; Accepts a pointer to a string, tries
; to read an unsigned number from its beginning.
; Returns in rax: a number, rdx : its length in characters
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor r10, r10			
	mov rcx, 10			; 10 in rcx we use as divisor
	xor rax, rax
	.loop:
		xor r11, r11
		mov r11b, byte[rdi + r10]
		cmp r11b, 0x39		
		ja .end			
		cmp r11b, 0x30			
		jb .end					
		inc r10			
		sub r11b, '0'		
		mul rcx			
		add rax, r11		
		jmp .loop
	.end:
		mov rdx, r10		
		ret




; Returns in rax: a number, rdx : its length in characters (including the sign, if there was one)
; rdx = 0 if the number could not be read
parse_int:
	cmp byte[rdi], '-'		
	jne .uint			
	inc rdi				
	call parse_uint			
	test rdx, rdx			
	jz .not_digit			
	neg rax				
	inc rdx				
	ret
	.uint:
		jmp parse_uint		
		ret
	.not_digit:
		xor rax, rax		
		ret


; Accepts a pointer to a string, a pointer to a buffer, and the length of the buffer
; Copies the string to the buffer
; Returns the length of the string if it fits in the buffer, otherwise 0
string_copy:
	push rdi
	push rsi
	push rdx
	call string_length ; the length of the string is stored in rax
	inc rax
	pop rdx
	pop rsi
	pop rdi
	cmp rax , rdx
	jg .less
	xor r11, r11
.loop:
	mov r11b, byte[rdi]
	mov byte[rsi], r11b
	cmp r11b, 0
	je .end
	inc rdi
	inc rsi
	jmp .loop
	
.less:
	xor rax, rax
	
.end:
	ret
	
	
